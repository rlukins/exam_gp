import * as THREE from 'three';
import * as CANNON from 'cannon-es';
import { CannonHelper } from './cannonHelper';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import dice1 from './textures/dice1.png';
import dice2 from './textures/dice2.png';
import dice3 from './textures/dice3.png';
import dice4 from './textures/dice4.png';
import dice5 from './textures/dice5.png';
import dice6 from './textures/dice6.png';
import sky1 from './textures/morning_ft.jpg';
import sky2 from './textures/morning_bk.jpg';
import sky3 from './textures/morning_up.jpg';
import sky4 from './textures/morning_dn.jpg';
import sky5 from './textures/morning_rt.jpg';
import sky6 from './textures/morning_lf.jpg';

class Main {
    constructor() {
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
        this.camera.position.set(30, 30, 30);

        const renderer = new THREE.WebGLRenderer({ antialias: true });
        renderer.setSize(window.innerWidth, window.innerHeight);
        this.renderer = renderer;
        document.body.appendChild(renderer.domElement);

        this.controls = new OrbitControls(this.camera, this.renderer.domElement);

        this.clock = new THREE.Clock();

        this.createPhysics();
        this.createLight();
        this.createSkybox();
        this.createPlane();
        this.createCube();
        this.animate();
    }
    createPhysics() {
        this.world = new CANNON.World();
        this.dt = 1.0 / 60.0;
        this.damping = 0.01;
        this.world.broadphase = new CANNON.NaiveBroadphase();
        this.world.gravity.set(0, -9.81, 0);
    }

    createLight() {
        this.scene.background = new THREE.Color(0x0d224a);
        const light = new THREE.HemisphereLight(0xeeeeff, 0x777788, 0.75);
        this.scene.add(light);
        light.position.set(0, 40, 0);
    }

    createSkybox() {
        var materials = [
            new THREE.MeshBasicMaterial({
                map: new THREE.TextureLoader().load(sky1) // front
            }),
            new THREE.MeshBasicMaterial({
                map: new THREE.TextureLoader().load(sky2) // back
            }),
            new THREE.MeshBasicMaterial({
                map: new THREE.TextureLoader().load(sky3) // up
            }),
            new THREE.MeshBasicMaterial({
                map: new THREE.TextureLoader().load(sky4) // down
            }),
            new THREE.MeshBasicMaterial({
                map: new THREE.TextureLoader().load(sky5) // right
            }),
            new THREE.MeshBasicMaterial({
                map: new THREE.TextureLoader().load(sky6) // left
            }),
        ];
        for (let i = 0; i < 6; i++) {
            materials[i].side = THREE.BackSide;
        }
        const skyboxgeo = new THREE.BoxGeometry(500, 500, 500);
        const skybox = new THREE.Mesh(skyboxgeo, materials);
        this.scene.add(skybox);
    }

    createPlane() {
        const geometry = new THREE.PlaneGeometry(100, 100, 1, 1);
        //const material = new THREE.MeshBasicMaterial({ color: 0xabcd54 });
        var material = new THREE.ShaderMaterial({  // Avots: http://glslsandbox.com/e#72401.0
            uniforms: this.uniforms,
            fragmentShader: `
            #extension GL_OES_standard_derivatives : enable
            #ifdef GL_ES
            precision mediump float;
            #endif
            uniform float time;
            uniform vec2 resolution;
            #define iTime time
            #define iResolution resolution
            float smin( float a, float b, float k ){
	            float h = clamp( 0.5 + 0.5*(b-a)/k, 0.0, 1.0 );
	            return mix( b, a, h ) - k*h*(1.0-h);
            }

            void mainImage( out vec4 fragColor, in vec2 fragCoord ){
                vec2 uv = (fragCoord-0.5*iResolution.xy)/iResolution.y;
                uv *= 100.;
                vec2 id = floor(uv);
                vec2 center = id + .5;
                vec2 st = fract(uv);
                float d = 1.;
                const float NNEI = 2.;
                for (float x = -NNEI; x <= NNEI; x++) {
                    for (float y = -NNEI; y < NNEI; y++) {
                        vec2 ndiff = vec2(x, y);
                        vec2 c = center + ndiff;
                        float r = length(c);
                        float a = atan(c.y, c.x);
                        r += sin(iTime * 5. - r*0.55) * min(r/5., 1.);
                        vec2 lc = vec2(r*cos(a), r*sin(a));
                        d = smin(d, length(uv - lc),0.65);
                    }
                }
                float w = fwidth(uv.y);
                vec3 col = vec3(smoothstep(0.31+w, 0.31-w, d));
	            col.r *= 0.6;
	            col.g *= 0.85;
                fragColor = vec4(col,1.0);
            }

            void main(void){
                mainImage(gl_FragColor, gl_FragCoord.xy);
            }`,
            vertexShader: `
            varying vec2 vUv;
            varying vec3 vNormal;

            void main() {
                vec4 modelViewPosition = modelViewMatrix * vec4(position, 1.0);
                vUv = uv;
                vNormal = normal;
                gl_Position = projectionMatrix * modelViewPosition; 
            }`
        });
        const plane = new THREE.Mesh(geometry, material);
        plane.material.side = THREE.DoubleSide;
        plane.rotation.x = Math.PI / 2;
        this.scene.add(plane);

        //const geometryphy = new CANNON.Plane();
        //const materialphy = new CANNON.Material({ color: 0xabcd54 });
        //const planeBody = new CANNON.Body({mass: 0, material: this.materialphy});
        //planeBody.quaternion.setFromAxisAngle(new CANNON.Vec3(1,0,0), Math.PI/2);
        //planeBody.addShape(geometryphy);
        //this.world.addBody(planeBody);
    }

    createCube() {
        var materials = [
            new THREE.MeshLambertMaterial({
                map: new THREE.TextureLoader().load(dice1)
            }),
            new THREE.MeshLambertMaterial({
                map: new THREE.TextureLoader().load(dice2)
            }),
            new THREE.MeshLambertMaterial({
                map: new THREE.TextureLoader().load(dice3)
            }),
            new THREE.MeshLambertMaterial({
                map: new THREE.TextureLoader().load(dice4)
            }),
            new THREE.MeshLambertMaterial({
                map: new THREE.TextureLoader().load(dice5)
            }),
            new THREE.MeshLambertMaterial({
                map: new THREE.TextureLoader().load(dice6)
            }),
        ];
        const geometry = new THREE.CubeGeometry(5, 5, 5, 1, 1, 1);
        const cube = new THREE.Mesh(geometry, materials);
        this.cube = cube;
        cube.position.set(0, 20, 0)
        this.scene.add(cube);

        this.size = (new THREE.Box3().setFromObject(this.cube)).getSize();
        this.cube.userData.rigidBody = new CANNON.Body({
            mass: 100,
            position: new CANNON.Vec3(this.cube.position.x,
                this.cube.position.y,
                this.cube.position.z),
            shape: new CANNON.Box(new CANNON.Vec3(this.size.x / 2, this.size.y / 2, this.size.z / 2))
        });
        //this.world.addBody(this.cube.rigidBody);
    }

    animate() {
        this.controls.update();
        this.world.step(this.dt);
        requestAnimationFrame(() => this.animate())
        this.renderer.render(this.scene, this.camera);
    }
}
(new Main());